<?php
/**
 * Plugin Name: Advertisements
 * Description: Add your own advertisements to your website.
 * Version: 1.0.0
 * Author: JeffL
 */

/**
 * Update Display
 */
if(!empty($_REQUEST['aDisplayed']) && $_REQUEST['aDisplayed'] == 'true')
{
    // update display
    display_plugin_update_advert_display($_REQUEST['aId']);
    // exit
    exit();
}

/**
 * Click Thru Redirect
 */
if(!empty($_REQUEST['aClicked']) && $_REQUEST['aClicked'] == 'true')
{
    // update click thru
    display_plugin_update_advert_clicked($_REQUEST['aId']);
    // redirect to url
    header('Location: '.$_REQUEST['redirect']);
    // exit
    exit();
}

/**
 * Init Plugin
 */
function display_plugin_adverts ( $atts = '' )
{
    // if we are in the admin area we dont need to load the front end plugin
    if (is_admin())
    {
        return;
    }

    // get url
    $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
    $url = "//".$_SERVER['HTTP_HOST'] . $uri_parts[0];

    // get all adverts
    $adverts = get_field('field_5dbc445277fad', 'option');

    // sort adverts array
    usort($adverts, 'sortArray');

    // load plugin template
    include("templates/adverts.php");
}
add_shortcode('show-advert', 'display_plugin_adverts');

/**
 * Sort Array By...
 * Use $a < $b to order by key DESC ( eg. 1, 2, 3 )
 * Use $a > $b to order by key ASC ( eg. 3, 2, 1 )
 */
function sortArray($a, $b)
{
    $a = $a['importance'];
    $b = $b['importance'];

    if ($a == $b) return 0;
    return ($a < $b) ? -1 : 1;
}

/**
 * Add Settings Link To Plugin Page
 */
function display_plugin_page_settings_link( $links ) {
    $links[] = '<a href="' . admin_url( 'options-general.php?page=display.php' ) . '">' . __('Settings') . '</a>';
    return $links;
}
add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'display_plugin_page_settings_link');

/**
 * Add ACF Options Page
 */
function display_plugin_register_acf_options_pages() {

    // check function exists
    if( !function_exists('acf_add_options_page') )
        return;

    // register options page
    $option_page = acf_add_options_page(array(
        'page_title'    => __('Advertisements'),
        'menu_title'    => __('Advertisements'),
        'menu_slug'     => 'display.php',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

    // register statistics page
    $statistics_page = acf_add_options_sub_page( array(
        'page_title'  => 'Statistics',
        'menu_title'  => 'Statistics',
        'parent_slug' => $option_page['menu_slug'],
        'capability' => 'manage_options',
        'menu_slug'   => 'advert-statistics',
    ) );
}

// Hook into acf initialization.
add_action('acf/init', 'display_plugin_register_acf_options_pages');

/**
 * Add ACF Options Sub Menu Statistics Page
 */
function display_plugin_register_acf_statistics_page()
{
    add_submenu_page(
        null,
        'Statistics',
        'Statistics',
        'manage_options',
        'advert-statistics',
        'display_plugin_register_acf_statistics_page_callback'
    );
}
add_action('admin_menu', 'display_plugin_register_acf_statistics_page');

/**
 * ACF Options Sub Menu Statistics Page Content
 */
function display_plugin_register_acf_statistics_page_callback()
{
    include('templates/statistics.php');
}

/**
 * Create Database Table
 */
function display_plugin_create_database_table ()
{
    global $wpdb;

    $table_name = $wpdb->prefix."_advertisement_stats";

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE IF NOT EXISTS $table_name (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `advert_id` int(11) DEFAULT NULL,
                `displays` int(11) DEFAULT 0,
                `click_thru` int(11) DEFAULT 0,
                `display_date` date DEFAULT NULL,
                `last_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
                ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

}
register_activation_hook( __FILE__, 'display_plugin_create_database_table' );

/**
 * Update Display Count
 */
function display_plugin_update_advert_display($advert_id = 0)
{
    global $wpdb;

    $date = date("Y-m-d");

    // set the table name
    $table_name = $wpdb->prefix."_advertisement_stats";

    // select
    $result = $wpdb->get_results("SELECT id 
                                         FROM ".$table_name."
                                         WHERE ( advert_id = '".(int)$advert_id."' AND display_date = '".$date."' )
                                         LIMIT 1");
    if(empty($result))
    {
        // add to table
        $wpdb->insert( $table_name, array(
            'advert_id' => (int)$advert_id,
            'displays' => 1,
            'display_date' => $date
        ));
    }
    else
    {
        // else update record
        $wpdb->query('UPDATE '.$table_name.' 
                             SET displays = displays+1 
                             WHERE ( advert_id = '.(int)$advert_id.' 
                             AND display_date = "'.$date.'" )');
    }
}

/**
 * Save Click Thru To Database
 */
function display_plugin_update_advert_clicked($advert_id = 0)
{
    global $wpdb;

    $date = date("Y-m-d");

    // set the table name
    $table_name = $wpdb->prefix."_advertisement_stats";

    // select
    $result = $wpdb->get_results("SELECT id 
                                         FROM ".$table_name."
                                         WHERE ( advert_id = '".(int)$advert_id."' AND display_date = '".$date."' )
                                         LIMIT 1");
    if(empty($result))
    {
        // add to table
        $wpdb->insert( $table_name, array(
            'advert_id' => (int)$advert_id,
            'click_thru' => 1,
            'display_date' => $date
        ));
    }
    else
    {
        // else update record
        $wpdb->query('UPDATE '.$table_name.' 
                             SET click_thru = click_thru+1 
                             WHERE ( advert_id = '.(int)$advert_id.' 
                             AND display_date = "'.$date.'" )');
    }
}