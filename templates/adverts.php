
<?php
$i = 0;
$allAdverts = "";
$allAdvertsID = "";
$allAdvertsURL = "";
if(!empty($adverts))
{
    foreach($adverts as $advert)
    {
        // get todays date
        $today = date("U");

        // get start date
        $sdate = explode("/", $advert['start_date']);
        $start_date = $sdate[2].'/'.$sdate[1].'/'.$sdate[0].' 00:00:00';
        $start_date = (!empty($sdate)) ? date("U", strtotime($start_date)) : 0;

        // get end date
        $edate = explode("/", $advert['end_date']);
        $end_date = $edate[2].'/'.$edate[1].'/'.$edate[0].' 23:59:59';
        $end_date = (!empty($edate)) ? date("U", strtotime($end_date)) : 0;

        // if advert start date is set to run in the future
        if(!empty($start_date) && $start_date > $today)
        {
           unset($adverts[$i]);
        }
        // if advert end date has passed
        else if(!empty($end_date) && $end_date < $today)
        {
            unset($adverts[$i]);
        }
        // else set values for display
        else
        {
            $allAdverts .= '"' . $advert['advert_img']['url'] . '",';
            $allAdvertsID .= '"' . $advert['advert_img']['id'] . '",';
            $allAdvertsURL .= '"' . urlencode($advert['advert_click_thru']) . '",';
        }

        // update count
        $i++;
    }
}

// re-index adverts array
$adverts = array_values($adverts);
?>


<div id="display">
    <?php if(!empty($adverts[0]['advert_img']['id'])) {?>
        <a id="clickUrl" class="clickUrl" href="<?php echo $url;?>?aClicked=true&aId=<?php echo $adverts[0]['advert_img']['id'];?>&redirect=<?php echo urlencode($adverts[0]['advert_click_thru']);?>" target="_blank">
            <img class="img img-fluid" src="<?php echo $adverts[0]['advert_img']['url'];?>">
        </a>
    <?php }?>
</div>


<script>
// variables
var url = '<?php echo $url;?>';
var adverts = [<?php echo substr($allAdverts, 0, -1);?>];
var advertsId = [<?php echo substr($allAdvertsID, 0, -1);?>];
var advertsUrl = [<?php echo substr($allAdvertsURL, 0, -1);?>];
var advertCount = 1;
var advertTotal = <?php echo count($adverts);?>;
var advertRefreshRate = <?php echo (int) ( get_field('refresh_rate', 'option') * 1000 );?>;
// if we have adverts to display
if(Number(advertTotal) >= 1)
{
    // onload, update display count
    jQuery.get(url + "?aDisplayed=true&aId=" + advertsId[0], function (data, status) {});
    // set interval (function)
    setInterval(function () {
        // if last advert, reset count
        if (advertCount >= advertTotal) {
            advertCount = 0;
        }
        // set advert html
        var advertHTML = '<a id="clickUrl" class="clickUrl" href="' + url + '?aClicked=true&aId=' + advertsId[advertCount] + '&redirect=' + advertsUrl[advertCount] + '" target="_blank"><img class="img img-fluid" src="' + adverts[advertCount] + '"></a>';
        // show advert
        document.getElementById('display').innerHTML = advertHTML;
        // update display count
        jQuery.get(url + "?aDisplayed=true&aId=" + advertsId[advertCount], function (data, status) {});
        // update count
        advertCount++;
    }, advertRefreshRate);
}
</script>
