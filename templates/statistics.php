
<style>
    .advert-stats-search {
        margin-top: 30px;
        border: 1px solid #a1a1a1;
        background-color: #fff;
        padding: 10px;
        margin-bottom: 30px;
    }
    .advert-stats-search-reset {
        margin-top: 30px;
        background-color: #fff;
        border-left: 4px solid #c3e6cb;
        padding: 15px;
        margin-bottom: 30px;
        box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    }
    .advert-stats-search-error {
        margin-top: 30px;
        background-color: #fff;
        border-left: 4px solid #f5c6cb;
        padding: 15px;
        margin-bottom: 30px;
        box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    }
    .advert_stats {
        width: 100%;
    }
    .advert_stats th {
        text-align: left;
        font-size: 16px;
    }
    .advert_stats td {
        width: 50%;
    }
    .advert_stats .advert-details div {
        margin-top: 10px;
    }
    .advert_stats .advert-details input {
        width: 100%;
    }
</style>

<script src="<?php echo plugins_url();?>/display/js/Chart.min.js"></script>

<div class="wrap">
    <form action="?page=advert-statistics" method="post">

    <h1>Advertising Statistics</h1>

    <?php
    global $wp,$wpdb;

    $adverts = get_field('field_5dbc445277fad', 'option');

    if(empty($adverts))
    {
    ?>
        <div class="advert-stats-search-error">
            <strong>No advertising data available</strong>
       </div>
    <?php
      return;
    }
    ?>

    <?php if(!empty($_GET['reset'])) {?>
        <div class="advert-stats-search-reset">
            <strong>Form has been reset</strong>
        </div>
    <?php }?>

    <?php if(empty($_POST['advert_campaign']) && empty($_GET['reset'])) {?>
       <div class="advert-stats-search-error">
            <strong>No Display Ad selected</strong>
       </div>
    <?php }?>

    <div class="advert-stats-search">
        <strong>Select a Display Ad</strong>:
            <select name="advert_campaign">
                <option value="">-- select --</option>
                <?php
                if(!empty($adverts))
                {
                    foreach ($adverts as $advert)
                    {
                        $selected = ($advert['advert_img']['id'] == $_POST['advert_campaign']) ? " selected='selected' " : 0;
                    ?>
                        <option value="<?php echo $advert['advert_img']['id'];?>" <?php echo $selected;?>><?php echo $advert['campaign_name'];?></option>
                    <?php
                    }
                }
                ?>
            </select>
            <input type="submit" value="Select" class="button" style="margin-top: 1px;">
            <input type="button" value="Reset" class="button" style="margin-top: 1px;" onclick="location.href = '//<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>&reset=true';">
            or <a href="?page=display.php">create a new Display Ad</a>
    </div>

        <?php
        // if no advert selected
        if(empty($_POST['advert_campaign']))
        {
            echo "<p></p>";
            return;
        }

        // validate start date string
        if (!empty($_POST['start_date']) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_POST['start_date']))
        {
            echo "<p>Error: Invalid Start Date format</p>";
            return;
        }
        // validate end date string
        if (!empty($_POST['end_date']) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_POST['end_date']))
        {
            echo "<p>Error: Invalid End Date format</p>";
            return;
        }

        // get search dates
        $advert_campaign = (!empty($_POST['advert_campaign'])) ? $_POST['advert_campaign'] : 0;
        $end = (!empty($_POST['end_date'])) ? $_POST['end_date']." 23:59:59" : date("Y-m-d");
        $start = (!empty($_POST['start_date'])) ? $_POST['start_date']." 00:00:00" : date('Y-m-d', strtotime('-1 month'));

        // get data for banner
        $results = $wpdb->get_results("SELECT display_date, displays, click_thru
                                              FROM wp__advertisement_stats
                                              WHERE advert_id = " . (int) $advert_campaign . "
                                              AND display_date BETWEEN '" . $start . "' AND '" . $end . "'
                                              ORDER BY display_date ASC");
        // set default values
        $advert_display_dates = array();
        $advert_displays = array();
        $advert_clicks = array();

        // foreach result in db
        if (!empty($results)) {
            foreach ($results as $result) {
                $advert_display_dates[] = date("d-m-Y", strtotime($result->display_date));
                $advert_displays[] = $result->displays;
                $advert_clicks[] = $result->click_thru;
            }
        }
        ?>

        <table class="advert_stats">
            <tr>
                <th>Display Ad |
                    <?php
                    foreach ($adverts as $advert)
                    {
                        if ($advert['advert_img']['id'] == $advert_campaign)
                        {
                            echo "".$advert['campaign_name']."";
                        }
                    }
                    ?>
                </th>
                <th>Ad Performance</th>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="text-align: center;"><input type="date" name="start_date" value="<?php echo $_POST['start_date'];?>" placeholder="yyyy-mm-dd"> to <input type="date" name="end_date" value="<?php echo $_POST['end_date'];?>" placeholder="yyyy-mm-dd"> <input type="submit" value="Show Results" class="button" style="margin-top: 2px;"></td>
            </tr>
            <tr class="advert-details">
                <td>
                    <?php
                    foreach ($adverts as $advert)
                    {
                        if ($advert['advert_img']['id'] == $advert_campaign)
                        {?>
                            <div>Published <?php echo date("d F Y", strtotime($advert['advert_img']['date']));?></div>
                            <div><img src="<?php echo $advert['advert_img']['sizes']['thumbnail'];?>"></div>
                            <div style="text-align: right;"><a href="?page=display.php">Edit</a></div>
                            <div>Banner Url: <input type="text" name="banner_url" value="<?php echo $advert['advert_img']['url'];?>" disabled readonly></div>
                            <div style="text-align: right;"><a href="?page=display.php">Update</a></div>
                            <div>Click Thru: <input type="text" name="click_thru" value="<?php echo $advert['advert_click_thru'];?>" disabled readonly></div>
                            <div style="text-align: right;"><a href="?page=display.php">Update</a></div>
                        <?php
                        }
                    }
                    ?>
                </td>
            <td>
                <canvas id="canvas_<?php echo $advert_campaign;?>"></canvas>
            </td>
            </tr>
        </table>

        <hr>

        <script>
            var chartColors = {
                red: 'rgb(255, 99, 132)',
                orange: 'rgb(255, 159, 64)',
                yellow: 'rgb(255, 205, 86)',
                green: 'rgb(75, 192, 192)',
                blue: 'rgb(54, 162, 235)',
                purple: 'rgb(153, 102, 255)',
                grey: 'rgb(201, 203, 207)'
            };

            var color = Chart.helpers.color;
            var barChartData = {
                labels: [<?php echo "'".implode("','",$advert_display_dates)."'";?>], // e.g. ['a', 'b', 'c']
                datasets: [ {
                    backgroundColor: color(chartColors.red).alpha(0.5).rgbString(),
                    borderColor: chartColors.red,
                    borderWidth: 1,
                    label: 'Total Displays',
                    data: [<?php echo implode(",",$advert_displays);?>], // e.g. [1, 2, 3]
                    order: 1
                },{
                    backgroundColor: color(chartColors.blue).alpha(0.5).rgbString(),
                    borderColor: chartColors.blue,
                    borderWidth: 1,
                    label: 'Click Thru',
                    data: [<?php echo implode(",",$advert_clicks);?>], // e.g. [1, 2, 3]
                    type: 'line',
                    order: 2
                }]
            };

            var ctx_<?php echo $advert_campaign;?> = document.getElementById('canvas_<?php echo $advert_campaign;?>').getContext('2d');
            window.myBar = new Chart(ctx_<?php echo $advert_campaign;?>, {
                type: 'bar',
                data: barChartData,
                options: {
                    title: {
                        display: true,
                        <?php
                        foreach ($adverts as $advert)
                        {
                            if ($advert['advert_img']['id'] == $advert_campaign)
                            {
                            ?>
                                text: '<?php echo $advert['campaign_name'];?> - Advert Displays & Click Thru Statistics'
                            <?php
                            }
                        }
                        ?>
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true
                }
            });
        </script>

    </form>
</div>